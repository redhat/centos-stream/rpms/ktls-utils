#!/bin/bash
MYOLDHOSTNAME=$(hostnamectl hostname --static)
MYHOSTNAME=nfs.ktls-utils.test
MYIP=$(ip -o route get to 8.8.8.8 | sed -n 's/.*src \([0-9.]\+\).*/\1/p')

echo "Setup..."
hostnamectl hostname --static "$MYHOSTNAME"
cp /etc/tlshd.conf /etc/tlshd.conf.bak
openssl req -x509 -newkey rsa:4096 -subj "/CN=ktls-utils smoketest CA" -days 365 -noenc -out ca-cert.pem -keyout ca-cert.key >/dev/null 2>&1
openssl req -x509 -newkey rsa:4096 -subj "/CN=${MYHOSTNAME}" -addext "subjectAltName=DNS:${MYHOSTNAME},IP:${MYIP}" -days 365 -noenc -CA ca-cert.pem -CAkey ca-cert.key -extensions usr_cert -out ktls.pem -keyout ktls.key >/dev/null 2>&1
cp ca-cert.pem /etc/pki/tls/certs
cp ktls.pem /etc/pki/tls/certs
cp ktls.key /etc/pki/tls/private

cat <<EOF >/etc/tlshd.conf
[debug]
loglevel=0
tls=0
nl=0

[authenticate]
#keyrings= <keyring>;<keyring>;<keyring>

[authenticate.client]
x509.truststore=/etc/pki/tls/certs/ca-cert.pem
x509.certificate=/etc/pki/tls/certs/ktls.pem
x509.private_key=/etc/pki/tls/private/ktls.key

[authenticate.server]
x509.truststore=/etc/pki/tls/certs/ca-cert.pem
x509.certificate=/etc/pki/tls/certs/ktls.pem
x509.private_key=/etc/pki/tls/private/ktls.key
EOF

systemctl start tlshd
systemctl start nfs-server
mkdir /export
exportfs -o rw,insecure,no_root_squash,xprtsec=tls:mtls *:/export

# mount by hostname
echo "Try to mount $MYHOSTNAME:/export without xprtsec=tls"
mount -o v4.2 $MYHOSTNAME:/export /mnt
if [ $? -eq 0 ]; then
	echo "Mounted $MYHOSTNAME:/export without xprtsec=tls!"
	exit 1
fi

echo "Try to mount $MYHOSTNAME:/export with xprtsec=tls"
mount -o v4.2,xprtsec=tls $MYHOSTNAME:/export /mnt
if [ $? -ne 0 ]; then
	echo "Failed to mount $MYHOSTNAME:/export with xprtsec=tls!"
	exit 1
fi

if ! grep "xprtsec=tls" /proc/mounts; then
	echo "Failed to find xprtsec=tls in /proc/mounts"
	exit 1
fi
umount /mnt

# mount by ip address
echo "Try to mount $MYIP:/export without xprtsec=tls"
mount -o v4.2 $MYIP:/export /mnt
if [ $? -eq 0 ]; then
	echo "Mounted $MYIP:/export without xprtsec=tls!"
	exit 1
fi

echo "Try to mount $MYIP:/export with xprtsec=tls"
mount -o v4.2,xprtsec=tls $MYIP:/export /mnt
if [ $? -ne 0 ]; then
	echo "Failed to mount $MYIP:/export with xprtsec=tls!"
	exit 1
fi

if ! grep "xprtsec=tls" /proc/mounts; then
	echo "Failed to find xprtsec=tls in /proc/mounts"
	exit 1
fi
umount /mnt

echo "Success!"

echo "Cleanup..."
hostnamectl hostname --static "$MYOLDHOSTNAME"
exportfs -ua
systemctl stop nfs-server
rmdir /export
systemctl stop tlshd
cp /etc/tlshd.conf.bak /etc/tlshd.conf
rm -f /etc/pki/tls/certs/ca-cert.pem
rm -f /etc/pki/tls/certs/ktls.pem
rm -f /etc/pki/tls/private/ktls.key
rm -f ca-cert.pem
rm -f ca-cert.key
rm -f ktls.pem
rm -f ktls.key

exit 0
